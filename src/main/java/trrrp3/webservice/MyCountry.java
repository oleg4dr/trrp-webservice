package trrrp3.webservice;


import java.io.Serializable;

public class MyCountry implements Serializable {
    private String name;
    private String capital;
    private int population;

    public MyCountry() {
    }

    @Override
    public String toString() {
        return "MyCountry{" +
                "name='" + name + '\'' +
                ", capital='" + capital + '\'' +
                ", population=" + population +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public MyCountry(String name, String capital, int population) {
        this.name = name;
        this.capital = capital;
        this.population = population;
    }
}
